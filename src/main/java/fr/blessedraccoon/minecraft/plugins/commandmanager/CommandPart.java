package fr.blessedraccoon.minecraft.plugins.commandmanager;

import java.util.Map;
import java.util.TreeMap;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * Represents all the information about a command <p>
 * Each part of a command is a CommandPart Instance.<p>
 * To add a subcommand, you need to add it using the addSubcommand(CommandPart)
 * method, in the constructor you define.
 * 
 * @see #addSubcommand(CommandPart)
 */
public abstract class CommandPart {

    /**
     * Map that associates a command name to a command
     */
    protected Map<String, CommandPart> subcommands = new TreeMap<>();
    
    /**
     * Returns the name of the command part <p>
     * i.e. : if your command is <code>/my_command subcommand</code>,
     * the first command's getName() will be <code>my_command</code>,
     * and the second will be <code>subcommand</code>.
     * 
     * @return name of the command part
     */
    public abstract String getName();

    /**
     * Returns the description of the command <p>
     * The description will be automatically displayed when the
     * <code>help</code> part of a command will be called.
     * 
     * @see HelpCommandPattern
     * @return the description of the command
     */
    public abstract String getDescription();

    /**
     * Returns the expected syntax for the function to work properly. <p>
     * The syntax will be automatically displayed when the
     * <code>help</code> part of a command will be called.
     * 
     * @see HelpCommandPattern
     * @return the expected syntax of the command
     */
    public abstract String getSyntax();

    /**
     * Returns how deep the command part is in the command chain. <p>
     * The Part right after <code>/</code> must return a deep level
     * of 0. This number increases by 1 for each command of a chain.
     * 
     * @return The deep level of the command part
     */
    public abstract int getDeepLevel();

    /**
     * Performs the action that the command must execute when called. <p>
     * @param player The player that executes the command
     * @param args arguments of the command (every word, split by " ",
     *              except the very first word, just after <code>/</code>)
     * @return returns true whether the command has successfully been executed.
     *         false otherwise.
     */
    public abstract boolean perform(Player player, String[] args);

    /**
     * Prints to the player a failure message, if the command is not well executed. <p>
     * The message can contain a red text, mentioning the expected command syntax
     * @see #getSyntax()
     * 
     * @param player player to print the error message to
     */
    public void fail(Player player) {
        player.sendMessage(ChatColor.RED + "Wrong usage ! Command syntax : " + this.getSyntax());
    }

    /**
     * Prints to the player a success message, if the command is well executed. <p>
     * The message can contain a green text, that sums up the action that has
     * been executed.
     * 
     * @param player player to print the error message to
     */
    public void succeed(Player player) {
        player.sendMessage(ChatColor.GREEN + "The command " + this.getSyntax() + " has run successfully !");
    }

    /**
     * Getter of subcommands
     * @return subcommands
     */
    public Map<String, CommandPart> getSubcommands() {
        return this.subcommands;
    }

    /**
     * Adds a subcommand to the map<p>
     * Automatically puts the name of the subcommand as a key
     * @param sub Subcommand to add
     */
    public void addSubcommand(CommandPart sub) {
        this.subcommands.put(sub.getName(), sub);
    }

    @Override
    public boolean equals(Object o1) {
        if (! (o1 instanceof CommandPart sc)) return false;
        return this.getName().equals(sc.getName());
    }

    @Override
    public String toString() {
        return this.getName();
    }
    
    public abstract String getPermission();

}
