package fr.blessedraccoon.minecraft.plugins.commandmanager;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * An automatically generating recap command<p>
 * To add a recap command to your command, you need to add
 * an instance of this class to subcommands AT THE END
 * of the CommandManager constructor, or just before the end
 * if there is a help command
 */
public abstract class RecapCommandValue extends CommandPart {

    /**
     * The command manager that contains the Help command,
     * so that it can identify all the other commands
     */
    private final CommandManager commandManager;

    /**
     * The list of all the commands in the command manager
     */
    private final List<String> commandValues = new ArrayList<>();

    /**
     * The main constructor. It automatically fills up the Valuable command list.
     * @param manager command manager where this
     *                recap command is in parameter
     */
    public RecapCommandValue(CommandManager manager) {
        this.commandManager = manager;
    }

    @Override
    public String getName() {
        return "recap";
    }

    @Override
    public String getDescription() {
        return "displays all commands that own a value";
    }

    @Override
    public String getSyntax() {
        return "/" + this.commandManager.getName() + " recap <number>" ;
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public void fail(Player player) {
        player.sendMessage(ChatColor.RED + "The page number that you asked does not exist.");
    }

    @Override
    public boolean perform(Player player, String[] args) {
        int index = 1;
        if (args.length > this.getDeepLevel()) {
            try {
                index = Integer.parseInt(args[this.getDeepLevel()]);
            } catch (NumberFormatException e) {
                return false;
            }
        }
        index--;
        boolean res = false;
        final int coeff = 7;
        int ponderedIndex = index * coeff;
        // Wipe the command recaps and calculate new ones
        this.commandValues.clear();
        this.crossCommands(this.commandManager);
        
        for (int i = ponderedIndex; i < ponderedIndex + coeff; i++) {
            if (i < this.commandValues.size()) {
                res = true;
                player.sendMessage((i + 1) + ") " + this.commandValues.get(i));
            }
        }
        if (res) {
            player.sendMessage("---------------- Page " + (index + 1) + "/" + ((this.commandValues.size() - 1) / (coeff) + 1) + " ---------------");
        }
        return res;
    }

    /**
     * Recursive method that lists all the commands of the subcommand tree
     * of the command manager, and add them all to the command list, if they
     * are Valuable.
     * @param currCommand The current command part that the method is
     *                    analysing (must be the command manager for
     *                    the first call)
     */
    private void crossCommands(CommandPart currCommand) {
        if (currCommand.getSubcommands().size() == 0) {
            this.addPattern(currCommand);
        }
        else {
            for (CommandPart cmd : currCommand.getSubcommands().values()) {
                crossCommands(cmd);
            }
        }
    }

    /**
     * Adds a pattern string to the commands list, from a Valuable command part
     * @param currCommand command part on which the pattern is built
     */
    private void addPattern(CommandPart currCommand) {
        if (currCommand instanceof CommandValuable comm) {
            this.commandValues.add(ChatColor.GREEN + currCommand.getSyntax() + " : " + ChatColor.WHITE + comm.getValue());
        }
    }

}
