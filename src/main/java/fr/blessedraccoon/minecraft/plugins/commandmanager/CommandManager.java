package fr.blessedraccoon.minecraft.plugins.commandmanager;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import net.md_5.bungee.api.ChatColor;
import org.jetbrains.annotations.NotNull;

/**
 * Class that handles executor and tabCompleter for a command. An instance of this class needs to be made
 * for every new command that you create. The subcommands can be completed with a CommandPart-inherited class
 * only.
 */
public abstract class CommandManager extends CommandPart implements CommandExecutor, TabCompleter {

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command cmd, @NotNull String label, String[] args) {
        if (sender instanceof Player player) {
            CommandPart toRun = this.getPerformingCommand(args);
            if (player.isOp() || toRun.getPermission() == null || player.hasPermission(toRun.getPermission())) {
                try {
                    boolean result = toRun.perform(player, args);
                    if (result) toRun.succeed(player);
                    else toRun.fail(player);
                } catch (Exception e) {
                    toRun.fail(player);
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "You can't perform this command, since you don't have the rights.");
            }
        }
        return true;
    }

    /**
     * Inside method that iterates over the matching subcommands, and return
     * the CommandPart object that is going to execute the given command.
     * @param args arguments of the command, everything after
     *             <code>/firstArg</code>, split by spaces
     * @return The CommandPart that will execute the given command
     */
    protected CommandPart getPerformingCommand(@NotNull String[] args) {
        if(args.length < 1) {
            return this;
        }
        int deepLevel = 0;
        CommandPart cmd = this;
        while (deepLevel < args.length) {
            CommandPart temp = cmd.subcommands.get(args[deepLevel]);
            if (temp == null) break;
            cmd = temp;
            deepLevel++;
        }
        return cmd;

    }

    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {
        if(args.length == 0) {
            return List.of(this.getName());
        }
        if (sender instanceof Player player) {
            CommandPart cmd = this.getPerformingCommand(args);
            int adapter = (args.length == cmd.getDeepLevel()) ? -1 : 0;
            String part1 = args[cmd.getDeepLevel() + adapter];
            List<String> part2 = cmd.getSubcommands().values().stream()
            		.filter(sub -> sub.getPermission() == null || player.hasPermission(sub.getPermission()) || player.isOp())
            		.map(CommandPart::getName)
            		.collect(Collectors.toList());
            return StringUtil.copyPartialMatches(part1, part2, new ArrayList<>());
        }
        return new ArrayList<>();
        
    }
    
}
