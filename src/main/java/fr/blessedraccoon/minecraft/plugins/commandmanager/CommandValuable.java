package fr.blessedraccoon.minecraft.plugins.commandmanager;

/**
 * If a command is linked to a value, it implements CommandValuable.
 * @author BlessedRaccoon
 */
@FunctionalInterface
public interface CommandValuable {
	
	/**
	 * Returns the linked value of the command
	 * @return value of the command
	 */
    String getValue();
}
